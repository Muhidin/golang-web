package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/PuerkitoBio/goquery"
)

type Product struct {
	Name  string
	Image string
	Price string
	//Url   string
	//Title string
}

func main() {
	res, err := http.Get("https://www.frozenshop.com/latest")
	//res, err := http.Get("https://news.detik.com/")

	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		log.Fatalf("status code error : %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	rows := make([]Product, 0)

	doc.Find(".grid").Children().Each(func(i int, sel *goquery.Selection) {
		row := new(Product)
		row.Image, _ = sel.Find(".wrap .struct .frame .image img").Attr("src")
		row.Name = sel.Find(".wrap .struct .frame .name a").Text()
		row.Price = sel.Find(".wrap .struct .frame .price").Text()

		rows = append(rows, *row)

	})

	// doc.Find(".list-content").Children().Each(func(i int, sel *goquery.Selection) {
	// 	row := new(Product)
	// 	row.Image, _ = sel.Find("article .ph_newsfeed_d .media .media__image .media__link .ratiobox img").Attr("src")
	// 	//row.Name = sel.Find(".name a").Text()
	// 	//row.Price = sel.Find(".price").Text()
	// 	row.Title = sel.Find("article .ph_newsfeed_d .media .media__text .media__title a").Text()
	// 	row.Url, _ = sel.Find("article .ph_newsfeed_d .media .media__text .media__title a").Attr("href")

	// 	rows = append(rows, *row)

	// })

	bts, err := json.MarshalIndent(rows, "", " ")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(string(bts))

}
