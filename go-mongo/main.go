package main

import (
	"context"
	"fmt"
	"log"
	"runtime"
	"sync"
	"time"

	. "github.com/gobeam/mongo-go-pagination"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var ctx = context.Background()

type student struct {
	Name  string `bson:"name"`
	Grade int    `bson:"Grade"`
}

type Product struct {
	Id       primitive.ObjectID `json:"_id" bson:"_id"`
	Name     string             `json:"name" bson:"name"`
	Quantity float64            `json:"qty" bson:"qty"`
	Price    float64            `json:"price" bson:"price"`
}

func connect() (*mongo.Database, error) {
	clientOptions := options.Client()
	clientOptions.ApplyURI("mongodb://localhost:27017")
	client, err := mongo.NewClient(clientOptions)

	if err != nil {
		return nil, err
	}

	err = client.Connect(ctx)

	if err != nil {
		return nil, err
	}

	return client.Database("belajar_golang"), nil
}

func insert() {
	db, err := connect()

	if err != nil {
		log.Fatal(err.Error())
	}

	_, err = db.Collection("student").InsertOne(ctx, student{"Wick", 2})
	if err != nil {
		log.Fatal(err.Error())
	}

	_, err = db.Collection("student").InsertOne(ctx, student{"Ethan", 2})
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println("Insert success")
}

func find(name string) {
	db, err := connect()
	if err != nil {
		log.Fatal(err.Error())
	}

	csr, err := db.Collection("student").Find(ctx, bson.M{"name": name})
	if err != nil {
		log.Fatal(err.Error())
	}

	defer csr.Close(ctx)

	result := make([]student, 0)
	for csr.Next(ctx) {
		var row student
		err := csr.Decode(&row)
		if err != nil {
			log.Fatal(err.Error())
		}

		result = append(result, row)

	}

	if len(result) > 0 {
		fmt.Println("Name :", result[0].Name)
		fmt.Println("Grade :", result[0].Grade)
	}

}

func update() {
	db, err := connect()
	if err != nil {
		log.Fatal(err.Error())
	}

	var selector = bson.M{"name": "Wick"}
	var change = student{"Jhon Wick", 2}

	_, err = db.Collection("student").UpdateOne(ctx, selector, bson.M{"$set": change})
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println("Update success")
}

func remove() {
	db, err := connect()
	if err != nil {
		log.Fatal(err.Error())
	}

	var selector = bson.M{"name": "Jhon Wick"}
	_, err = db.Collection("student").DeleteOne(ctx, selector)

	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println("Remove Success")
}

func pagination() {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		panic(err)
	}

	var limit int64 = 10
	var page int64 = 1
	collection := client.Database("belajar_golang").Collection("stocks")

	match := bson.M{"$match": bson.M{"qty": bson.M{"$gt": 1}}}

	projectQuery := bson.M{"$project": bson.M{"_id": 1, "qty": 1}}

	aggPaginatedData, err := New(collection).Context(ctx).Limit(limit).Page(page).Sort("price", -1).Aggregate(match, projectQuery)
	if err != nil {
		panic(err)
	}

	var aggProductList []Product
	for _, raw := range aggPaginatedData.Data {
		var product *Product
		if marshallErr := bson.Unmarshal(raw, &product); marshallErr == nil {
			aggProductList = append(aggProductList, *product)
		}

	}

	// print ProductList
	fmt.Printf("Aggregate Product List: %+v\n", aggProductList)

	// print pagination data
	fmt.Printf("Aggregate Pagination Data: %+v\n", aggPaginatedData.Data)
}

func insert_wg(wg *sync.WaitGroup, data string) {
	defer wg.Done()
	db, err := connect()

	if err != nil {
		log.Fatal(err.Error())
	}

	_, err = db.Collection("student").InsertOne(ctx, student{"Wick" + data, 2})
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println("Insert success")
}

func createJSON(wg *sync.WaitGroup, data string) {
	defer wg.Done()

}

func main() {
	//insert()
	//find("Jhon Wick")
	// find("Ethan")
	//update()
	//remove()
	// pagination()
	runtime.GOMAXPROCS(2)

	var wg sync.WaitGroup

	for i := 0; i < 10000; i++ {
		var data = fmt.Sprintf("%d", i)

		wg.Add(1)

		go insert_wg(&wg, data)
		//go simpanData(&wg, data)
	}

	wg.Wait()
}
