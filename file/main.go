package main

import (
	"fmt"
	"os"
)

func path_nya() (path string, err error) {

	dir, err := os.Getwd()

	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}

	return dir, nil
}

func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}
	return (err != nil)
}

func createFile() {

	path, err := path_nya()
	if err != nil {
		fmt.Println(err.Error())
	}
	new_path := path + "/test.txt"

	fmt.Println(new_path)

	// cek apakah path sudah ada
	var _, err2 = os.Stat(new_path)

	if os.IsNotExist(err2) {
		var file, err = os.Create(new_path)

		if isError(err) {
			return
		}
		defer file.Close()
	}

	fmt.Println("===> file created", path)
}

func writeFile() {
	path, err := path_nya()
	if err != nil {
		fmt.Println(err.Error())
	}
	new_path := path + "/test.txt"

	var file, err2 = os.OpenFile(new_path, os.O_RDWR, 0644)
	if isError(err2) {
		return
	}
	defer file.Close()

	// tulis data ke file
	var first_name string = "Arkananta"
	var last_name string = "Wijaya"
	_, err = file.WriteString("muhidin|saja\n")
	if isError(err) {
		return
	}

	_, err = file.WriteString("aufa|rijal\n")
	if isError(err) {
		return
	}

	_, err = file.WriteString(first_name + "|" + last_name + "\n")
	if isError(err) {
		return
	}

	// simpan perubahan
	err = file.Sync()
	if isError(err) {
		return
	}

	fmt.Println("==> file berhasil diisi")
}

func main() {
	createFile()
	writeFile()
}
