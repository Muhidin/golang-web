package main

import (
	"fmt"
	"runtime"

	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

func ConnectDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:pa$$word01@tcp(127.0.0.1:3306)/test_yaa")

	if err != nil {
		return nil, err
	}

	return db, nil
}

func cetak_data(message string) {
	fmt.Println(message)
}

func simpanData(message string) {
	db, err := ConnectDB()
	if err != nil {
		fmt.Println("Error")
		return
	}

	fmt.Println("database connected")
	defer db.Close()

	_, err = db.Exec("INSERT INTO test_chanel (text) VALUES (?) ", message)

	if err != nil {
		fmt.Println("Simpan data error")
		return
	}

	fmt.Println("Simpan data berhasil")
}

func main() {
	runtime.GOMAXPROCS(2)

	// db, err := ConnectDB()
	// if err != nil {
	// 	fmt.Println("Error")
	// 	return
	// }

	// fmt.Println("database connected")
	// defer db.Close()

	//messages := make(chan int)
	data := make(chan string)

	go func() {

		for {
			//i := <-messages
			data2 := <-data
			//fmt.Println("terima data" + data2)
			//fmt.Println("receive data", i)
			//cetak_data("terima data" + data2)
			simpanData(data2)
		}
	}()

	for i := 0; i < 1000001; i++ {
		//fmt.Println("send data", i)
		datanya := fmt.Sprintf("ke %d ", i)
		fmt.Println("kirim data " + datanya)
		//messages <- i
		data <- datanya
	}
}
