package main

import (
	"fmt"
	"net/http"
	"rest-api-mongo/routes"
)

func main() {
	routes.Routes()
	fmt.Println("Web server started at localhost:9000")
	http.ListenAndServe(":9000", nil)
}
