package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	. "rest-api-mongo/db"
	. "rest-api-mongo/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var ctx = context.Background()
var table = "products"

func Products(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var request = r.Method

	switch request {
	case "GET":
		//fmt.Println("Get method")
		var code = r.URL.Query().Get("c")
		var page = r.URL.Query().Get("page")
		var perpage = r.URL.Query().Get("perpage")

		if page != "" && perpage != "" {

			var pagenya, err = strconv.Atoi(page)

			if err != nil {
				fmt.Println("Convert error")
				return
			}

			var perpagenya, err2 = strconv.Atoi(perpage)

			if err2 != nil {
				fmt.Println("Convert error")
				return
			}

			var skip = pagenya
			if skip > 0 {
				skip = skip * perpagenya
			}

			//fmt.Printf("skip: %d pagenya: %d", skip, perpagenya)\
			db, err := Connect()
			if err != nil {
				http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
				return
			}

			skipnya := skip
			limitnya := perpagenya

			opts := options.FindOptions{
				Skip:  skipnya,
				Limit: limitnya,
			}

			csr, err := db.Collection(table).Find(nil, nil, &opts)
			if err != nil {
				http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
				return
			}

			defer csr.Close(ctx)
			var res_prod []Product

			for csr.Next(ctx) {
				var row Product
				var err = csr.Decode(&row)

				if err != nil {
					http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
					return
				}

				res_prod = append(res_prod, row)
			}

			var obj = bson.M{"Error": false, "Message": "Get data success", "Data": res_prod}
			var res, err2 = json.Marshal(obj)
			if err != nil {
				http.Error(w, `{"Error": true, "Message":"`+err2.Error()+`"}`, http.StatusInternalServerError)
				return
			}

			w.Write(res)
			return
		}

		if code != "" {
			db, err := Connect()
			if err != nil {
				http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
				return
			}

			csr, err := db.Collection(table).Find(ctx, bson.M{"code": code})
			if err != nil {
				http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
				return
			}

			defer csr.Close(ctx)
			var res_prod []Product

			for csr.Next(ctx) {
				var row Product
				var err = csr.Decode(&row)

				if err != nil {
					http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
					return
				}

				res_prod = append(res_prod, row)
			}

			var obj = bson.M{"Error": false, "Message": "Get data success", "Data": res_prod}
			var res, err2 = json.Marshal(obj)
			if err != nil {
				http.Error(w, `{"Error": true, "Message":"`+err2.Error()+`"}`, http.StatusInternalServerError)
				return
			}

			w.Write(res)
			return

		}

	case "POST":
		//fmt.Println("post method")
		var code = r.FormValue("code")
		var name = r.FormValue("name")
		var price = r.FormValue("price")
		var unit = r.FormValue("unit")
		var stock = r.FormValue("stock")
		var cat_id = r.FormValue("category")

		if code == "" || name == "" || price == "" {
			var data = M{"Error": true, "Message": "Field cannot be null"}

			var res, err2 = json.Marshal(data)
			if err2 != nil {
				http.Error(w, `{"Error": true, "Message":"`+err2.Error()+`"}`, http.StatusInternalServerError)
				return
			}
			w.Write(res)
			return
		}

		database, err := Connect()
		if err != nil {
			log.Fatal(err.Error())
		}

		_, err = database.Collection(table).InsertOne(ctx, Product{code, name, price, unit, stock, cat_id})
		if err != nil {
			//http.Error(w, `{"Error": true, "Message":"`+err.Error()+`"}`, http.StatusInternalServerError)
			fmt.Println("Error")
			return
		}

		var obj = M{"Error": false, "Message": "Success insert data"}
		// w.Write(result)
		var res, err2 = json.Marshal(obj)
		if err2 != nil {
			http.Error(w, `{"Error": true, "Message":"`+err2.Error()+`"}`, http.StatusInternalServerError)
			return
		}
		w.Write(res)
		return

	case "PUT":
		fmt.Println("put method")

	case "DELETE":
		fmt.Println("DELETE method")
	}
}
