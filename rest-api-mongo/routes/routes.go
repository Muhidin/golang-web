package routes

import (
	"net/http"
	"rest-api-mongo/controllers"
)

func Routes() {

	http.HandleFunc("/api/v1/product", controllers.Products)
}
