package models

type M map[string]interface{}

type Product struct {
	Code     string `bson:"code"`
	Name     string `bson:"name"`
	Price    string `bson:"price"`
	Unit     string `bson:"unit"`
	Stock    string `bson:"stock"`
	Category string `bson:"category"`
}

type Category struct {
	Category_id   int    `bson:"category_id"`
	Category_name string `bson:"category_name"`
}
