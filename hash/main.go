package main

import (
	"crypto/sha1"
	"fmt"
)

func doHash(text string) (string, string) {
	var salt = "){}<?-0(**&^E!@#$%$$^*(*_==+/?~1-9_A-Z"
	var saltedText = fmt.Sprintf("text: %s, salt: %s", text, salt)
	fmt.Println(saltedText)

	var sha = sha1.New()
	sha.Write([]byte(saltedText))
	var encrypted = sha.Sum(nil)

	return fmt.Sprintf("%x", encrypted), salt
}

func main() {
	var text = "this is screet"
	fmt.Printf("original :%s\n\n", text)

	var hashed1, salt1 = doHash(text)
	fmt.Printf("Hashed 1 : %s\n\n", hashed1)

	_ = salt1
}
