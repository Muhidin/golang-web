package controllers

import (
	"context"
	"log"
	"time"

	"github.com/gofiber/fiber/v2"

	"fiber-mongo/config"
	"fiber-mongo/models"

	"github.com/golang-jwt/jwt/v4"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"
)

func verifyPassword(hashedPassword string, password string) (isPasswordValid bool) {

	if err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)); err != nil {
		return false
	}

	return true
}

func hashPassword(password string) (hashedPassword string) {

	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}

	return string(hash)

}

func RegisterUser(c *fiber.Ctx) error {
	userCollection := config.MI.DB.Collection("users")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	user := new(models.User)

	if err := c.BodyParser(user); err != nil {
		log.Println(err)
		return c.Status(400).JSON(fiber.Map{
			"success": false,
			"message": "Failed to parse body",
			"error":   err,
		})
	}

	user.Password = hashPassword(user.Password)
	user.Created_at, _ = time.Parse(time.RFC3339, time.Now().Format(time.RFC3339))
	user.Updated_at, _ = time.Parse(time.RFC3339, time.Now().Format(time.RFC3339))

	countUsername, err2 := userCollection.CountDocuments(ctx, bson.M{"username": user.Username})
	if err2 != nil {
		return c.Status(500).JSON(fiber.Map{
			"success": false,
			"message": "Have error",
			"error":   err2,
		})
	}

	if countUsername > 0 {
		return c.Status(200).JSON(fiber.Map{
			"success": false,
			"message": "username has been used",
		})
	}

	countEmail, err3 := userCollection.CountDocuments(ctx, bson.M{"email": user.Email})
	if err2 != nil {
		return c.Status(500).JSON(fiber.Map{
			"success": false,
			"message": "Have error",
			"error":   err3,
		})
	}

	if countEmail > 0 {
		return c.Status(200).JSON(fiber.Map{
			"success": false,
			"message": "Email has been used",
		})
	}

	result, err := userCollection.InsertOne(ctx, user)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"success": false,
			"message": "User failed to insert",
			"error":   err,
		})
	}

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{
		"data":    result,
		"success": true,
		"message": "User insert successfully",
	})

}

func Login(c *fiber.Ctx) error {
	userCollection := config.MI.DB.Collection("users")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var user models.User
	username := c.FormValue("username")
	password := c.FormValue("password")

	// hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(hashedPassword)

	findRes := userCollection.FindOne(ctx, bson.M{
		"username": username,
	})

	if err := findRes.Err(); err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"success": false,
			"message": "User not found",
			"error":   err,
		})
	}

	findRes.Decode(&user)

	// cek passwordnya
	cek := verifyPassword(user.Password, password)
	//fmt.Println(cek)

	if cek == true {

		// return c.Status(fiber.StatusOK).JSON(fiber.Map{
		// 	"success": true,
		// 	"message": "Login success",
		// })

		token := jwt.New(jwt.SigningMethodHS256)

		// SET Claims
		claims := token.Claims.(jwt.MapClaims)
		claims["username"] = user.Username
		claims["firstname"] = user.Firstname
		claims["lastname"] = user.Lastname
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

		t, err := token.SignedString([]byte(config.Config("SCREET_KEY")))
		if err != nil {
			return c.SendStatus(fiber.StatusInternalServerError)
		}

		var t2 = t
		//user.Token = t2
		// fmt.Println(user.Token)

		user2 := new(models.User)

		user2.Token = t2
		user2.Created_at = user.Created_at
		user2.Updated_at, _ = time.Parse(time.RFC3339, time.Now().Format(time.RFC3339))

		update := bson.M{
			"$set": user2,
		}

		_, err = userCollection.UpdateOne(ctx, bson.M{"username": username}, update)
		if err != nil {
			return c.Status(500).JSON(fiber.Map{
				"success": false,
				"message": "Cannot create token",
				"error":   err.Error(),
			})
		}

		return c.JSON(fiber.Map{
			"token":   t,
			"success": true,
			"message": "Login success",
		})

	} else {
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"success": false,
			"message": "Login failed, please cek username and password!",
		})
	}

}
