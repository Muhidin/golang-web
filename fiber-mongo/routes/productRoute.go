package routes

import (
	"fiber-mongo/controllers"

	"github.com/gofiber/fiber/v2"

	"fiber-mongo/middleware"
)

func ProductRoute(route fiber.Router) {
	route.Get("/", middleware.Protected(), controllers.GetAllProduct)
	route.Get("/:id", middleware.Protected(), controllers.GetProduct)
	route.Post("/", middleware.Protected(), controllers.AddProduct)
	route.Put("/:id", middleware.Protected(), controllers.UpdateProduct)
	route.Delete("/:id", middleware.Protected(), controllers.DeleteProduct)
}
