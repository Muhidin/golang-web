package routes

import (
	"fiber-mongo/controllers"

	"fiber-mongo/middleware"

	"github.com/gofiber/fiber/v2"
)

func UserRoute(route fiber.Router) {
	route.Post("/register", middleware.Protected(), controllers.RegisterUser)
	route.Post("/login", controllers.Login)
}
