package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	Id            primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Username      string             `json:"username,omitempty" bson:"username,omitempty"`
	Firstname     string             `json:"firstname" bson:"firstname"`
	Lastname      string             `json:"lastname" bson:"lastname"`
	Email         string             `json:"email,omitempty" bson:"email,omitempty"`
	Phone         string             `json:"phone" bson:"phone"`
	Token         string             `json:"token" bson:"token"`
	Refresh_token string             `json:"refresh_token" bson:"refresh_token"`
	Password      string             `json:"password,omitempty" bson:"password,omitempty"`
	Created_at    time.Time          `json:"created_at", bson:"created_at"`
	Updated_at    time.Time          `json:"updated_at", bson:"updated_at"`
}
