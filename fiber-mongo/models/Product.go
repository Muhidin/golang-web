package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Product struct {
	Id    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name  string             `json:"name,omitempty" bson:"name,omitempty"`
	Price int                `json:"price,omitempty" bson:"price,omitempty"`
	Unit  string             `json:"unit,omitempty" bson:"unit,omitempty"`
	Stock int                `json:"stock,omitempty" bson:"stock,omitempty"`
}
