module fiber-mongo

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/gofiber/fiber/v2 v2.19.0 // indirect
	github.com/gofiber/jwt/v2 v2.2.7 // indirect
	github.com/gofiber/jwt/v3 v3.1.1 // indirect
	github.com/golang-jwt/jwt/v4 v4.1.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/valyala/fasthttp v1.30.0 // indirect
	go.mongodb.org/mongo-driver v1.7.2 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20210925032602-92d5a993a665 // indirect
)
