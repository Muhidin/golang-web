package fungsi

import (
	"fmt"
	"strings"
)

func TarikTambang(inputnya string) {

	asRune := []rune(inputnya)

	if len(asRune) > 1 {
		pjg := len(asRune)
		kiri := len(asRune) / 2

		dataKiri := asRune[0:kiri]
		dataKanan := asRune[kiri:pjg]

		var arrKiri = strings.Split(string(dataKiri), "")
		var arrKanan = strings.Split(string(dataKanan), "")

		var jmlKiri, jmlKanan, a, b int
		for _, each1 := range arrKiri {

			if each1 == "A" {
				a = 1
			} else if each1 == "B" {
				a = 2
			} else if each1 == "C" {
				a = 3
			} else if each1 == "D" {
				a = 4
			} else {
				a = 0
			}

			jmlKiri += a
		}

		for _, each2 := range arrKanan {

			if each2 == "A" {
				b = 1
			} else if each2 == "B" {
				b = 2
			} else if each2 == "C" {
				b = 3
			} else if each2 == "D" {
				b = 4
			} else {
				b = 0
			}

			jmlKanan += b
		}

		if jmlKiri == jmlKanan {
			fmt.Println("Sama sama kuat")
		}

		if jmlKiri > jmlKanan {
			fmt.Println("Kiri menang")
		}

		if jmlKanan > jmlKiri {
			fmt.Println("Kanan menang")
		}
	} else {
		fmt.Println("Sama sama kuat")
	}

}
