package fungsi

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Book struct {
	Title string `json:"title"`
	Price int    `json:"price"`
	Promo int    `json:"promo"`
}

type Result struct {
	Catalog string `json:"catalog"`
	Book    []Book `json:"book"`
}

type Hasilnya struct {
	Catalog    string `json:"catalog"`
	Totalprice int    `json:"totalprice"`
	Totalpromo int    `json:"totalpromo"`
}

func GetJson() {
	var result []Result

	url := "https://api.jsonbin.io/b/6108111bf14b8b153e05f501"
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
		return
	}
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	err = json.NewDecoder(res.Body).Decode(&result)

	if err != nil {
		fmt.Println("Error")
	}

	var hasilnya []Hasilnya
	var ttlprice int
	var ttlpromo int

	for _, each := range result {
		var catalog = fmt.Sprintf("%s", each.Catalog)

		ttlprice = 0
		ttlpromo = 0

		for _, each2 := range each.Book {

			ttlprice += each2.Price
			ttlpromo += each2.Promo

		}

		totalPrice := ttlprice
		totalPromo := ttlpromo

		var datanya Hasilnya

		datanya.Catalog = catalog
		datanya.Totalprice = totalPrice
		datanya.Totalpromo = totalPromo

		hasilnya = append(hasilnya, datanya)

	}

	var jsonData, err2 = json.Marshal(hasilnya)
	if err2 != nil {
		fmt.Println(err2.Error())
		return
	}

	var jsonString = string(jsonData)
	fmt.Println(jsonString)
}
